% !TeX encoding = UTF-8
% !TeX TS-program = pdflatex
% !TeX spellcheck = en_US

\documentclass[10pt,draft,journal,comsoc]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{algorithm}
\usepackage{algpseudocode}

%Importar las gráficas
\usepackage{pgfplots}
\usepgfplotslibrary{groupplots}

%Ecuaciones, y align
\usepackage{amsmath}

%Tablas muy grandes
\usepackage{longtable}

%Para figuras
\usepackage{graphicx}

%Citar por autor
%\usepackage[numbers]{natbib}

%Creando el comando \When en algorithmic
\algblockdefx[When]{WhenStart}{WhenEnd}%
[2][Unknown]{\textbf{when} #1 is received from #2 do: }%
{\textbf{end when}}


% *** CITATION PACKAGES ***
%
\usepackage{cite}


%Improves table formatting
\usepackage{array}
\usepackage[caption=false,font=footnotesize]{subfig}

\bibliographystyle{IEEEtran}


\begin{document}
\title{Routing in Intermittently Connected Mobile Networks with probabilistic soft-state}

\author{Jairo J. S\'anchez-Hern\'andez, Rolando Menchaca-M\'endez, Ricardo Menchaca-M\'endez, Mario E. Rivero-\'Angeles, J.J. Garc\'ia-Luna-Aceves and Jes\'us Garc\'ia-D\'iaz

\thanks{Manuscript received November 15, 2018; revised November 26, 2018.}%
\thanks{This work was partly funded by CONACyT.}}


\markboth{Transactions on Wireless Communications, Vol. 0, No. 0, January 2018}{}

\IEEEpubid{0000--0000/00\$00.00 \copyright 2019 IEEE}

\maketitle

\begin{abstract}
	In this paper, we present a novel protocol for routing in intermittently connected networks that gives any node in the network the ability to calculate a delivery probability for any other node by exchanging constant-size Counting Bloom filters.  We also propose a set of operations for the filters that create a gradient for the meeting probabilities of each node, namely the unary degradation operation that models the loss of topological information throughout the network hops and time; and the binary addition operation that is used to acquire topological information from neighboring nodes. These two operations create a probabilistic soft-state with only the contents of the Bloom filters. We present a series of experimental results based on extensively detailed simulations that show that the proposed protocol closely competes with the state of the art protocols by delivering more data packets with less overhead while being highly competitive with the latency and outperforming in overall complexity in MANET, pedestrian and VANET scenarios. This paper extends previous work presented in \cite{MSWiMpaper}.
\end{abstract}


\section{Introduction}
Intermittently Connected Networks (ICNs) are a generalization of the mobile ad hoc networks (MANETs) that does not assume the existence of contemporaneous end-to-end paths connecting any pair of nodes in the network causing constant network fragmentation. In this type of networks, sources and destinations could be disconnected for arbitrarily long periods of time; therefore, the applications running in this type of networks must be delay tolerant. Hence, they are also named as Delay Tolerant Networks (DTNs) \cite{zhang2006routing}. ICNs are a convenient alternative to provide communication services in challenging situations like difficult terrain, economic reasons or temporal constraints \cite{KFall04}. Examples of such scenarios are search and rescue missions; networks used to collect migration data of wildlife \cite{Juang02}; information dissemination in sparse vehicular ad hoc networks (VANETs) \cite{cao2013routing, li2007routing, wisitpongphan2007routing} and even interplanetary communication \cite{KFall04}.
We define the model for an ICN as a time-varying directed multi-graph where each node represents a communication device, and each edge represents a particular type of link with an associated capacity. Multiple edges exist in the case where a node has various types of network interfaces such as a satellite link or a modem-based telephonic link. In general, the capacity of a link is a time-varying function that takes values ranging from zero capacity when the connection is not available, to a predefined maximum capacity that reflects the nature of the link. This way, an edge $e_n$ connecting nodes $u, v$ in the multi-graph indicates that there is a link between nodes $u, v$ in the intermittently connected network. An edge (link) can be defined by $e = ((u, v), c_e (t), d_e (t))$ where $c_e(t)$ is a time-varying capacity function, $d_e (t)$ is a time-varying delay function and $(u, v)$ is an ordered pair that indicates the direction of the link.
The problem of routing in ICNs can be defined as follows. Given a time-varying multigraph $G(t) =(V (t), E(t))$ and two nodes $o, d \in V(t)$ find a sequence of operations $\gamma_1 \gamma_2 . . . \gamma_n$ that provides a path for a packet generated in the origin node $o$ to the destination $d$. The set of $\gamma_i$ operations are part of the alphabet $\Gamma$ that we define in Table \ref{tab:ops} abstracting the characteristic operations in ICNs.

\begin{table}[!t]
	\renewcommand{\arraystretch}{1.3}
	\caption{$\Gamma$ alphabet}
	\label{tab:ops}
	\centering
	\begin{tabular}{c||c}
		\hline
		\bfseries Operation & \bfseries Description\\
		\hline\hline
		$\gamma_{discover}$ & Discover neighbors\\
		\hline
		$\gamma_{store}$    & Stores message in buffer\\
		\hline
		$\gamma_{tx}$       & Transmits a message to neighbor\\
		\hline
		$\gamma_{copy}$     &	Make a copy of a message\\
		\hline
	\end{tabular}
\end{table}


\section{Related work}
\label{sec:related}
We present a small but representative sample of the papers that have addressed the problem of routing in intermittently connected networks. We highlight the fact that previous work establishes individual orderings over the nodes per each of the destinations in the network, and that these orderings are either based on utility functions or probability functions. This approach increases the spatial and time complexity, making it unviable for large-scale networks and less flexible in situations where the network diminishes in size. Unlike these approaches, we propose a protocol that simultaneously establishes a probability function for all the destinations using a single constant-size control packet. This property makes the proposed protocol more scalable without increasing the complexity.

A. Vahdat proposed the Epidemic routing algorithm \cite{Vahdat00} in 2000 as a simple solution to the problem of routing in delay tolerant networks (DTN). The method used by this algorithm is analogous to an infection, where packets represent the disease. On each encounter, the carrier nodes copy and transmit the packet if the neighboring node does not have that packet. This way, data packets can spread out very quickly through the connected segments of the network. From this point on, the algorithm exploits the node’s mobility to reach other network segments and keep spreading the packets until they reach their intended destination. This propagation model always finds the fastest route for every packet if the nodes have infinite bandwidth and storage, this because Epidemic uses every link available to disseminate copies and therefore it is a brute force algorithm. In the following cases, Epidemic fails to deliver packets: the destination is in a completely isolated network component, the destination does not even exist, or in a more realistic scenario, when the nodes discard packets at the data queues. There have been several derivatives of Epidemic such as EM-MA \cite{choksatid2015epidemic} or EPI-T \cite{EPI-T} that try to alleviate the problems of Epidemic, unfortunately, the scalability problems remain on both variants.

PRoPHET \cite{Lindgren03} is a probability-based routing protocol for DTNs that take advantage of the repetitive behavioral patterns of the human users. For example, if a node has visited a location many times in the past, it can be assumed that there is a high probability that this node will be there in the future. PRoPHET employs a metric called delivery probability $P(u,v) \in [0, 1]$ that indicates how likely is that a node $u$ can deliver messages to node $v$. Each node keeps track of every other node has met deriving in a list of delivery probabilities. Whenever two nodes come into transmission range, they exchange summaries that contain the list of packets each currently has and the list of their delivery probabilities. The information of the summary is then used to request those packets for which the current node is a better forwarder.


In \cite{boice2009combining}, Boice et al. propose the space-content adaptive-time routing (SCaTR) framework for routing in DTN MANETs. SCaTR is an extension of traditional on-demand routing that takes action when the protocol is unable to establish a connected route. When in DTN mode, SCaTR uses contact values to select the best node that can act as a proxy of the destination and store packets at that node until either the destination is discovered, or another node is selected as a better proxy. Contact values are computed based on the past connectivity information that nodes keep in their contact tables.

In Spray and Wait \cite{spyropoulos2008efficientSingle, spyropoulos2008efficient}, when a node creates a message, it assigns a fixed number of copies left to the message, if the number of copies $L$ reaches 1, the node enters in direct transmission mode. Variations on the heuristic to disseminate the copies include the first $L$ contacts the origin node met, randomized set of $L$ nodes, and the most used binary Spray and Wait where a node transfers $\lfloor L/2 \rfloor$ copies on each new contact until it has only one copy left.

SeeR \cite{SeeR} uses a heuristic based in simulated annealing to optimize the routes, with a strong focus in minimizing the end-to-end delay of network messages through the minimization of the route length. For a more comprehensive analysis of a large amount of work reporting routing protocols for intermittently connected networks, please refer to the surveys by Benamar et al. \cite{benamar2014routing}  and Cao and Sun \cite{cao2013routing}.

Bloom filters on any of their many variants have a large number of applications for distributed systems such as caching, peer-to-peer networks, routing and forwarding, monitoring and measurement and
data summarization \cite{tarkoma2012theory}. Their use as a tool to compute routes is less extensive, mainly because of the complications introduced by false positives \cite{sarela2011forwarding}.

However, recent proposals such as \cite{reinhardt2012cbfr, Zheng2014IWQoS} can improve in these complications by either assuming or inducing a tree-like topology.

Bloom characterized the Bloom filter false positive rate \cite{Bloom70}, but recent works \cite{BF_BOSE_2008, BF_CHRISTENSEN_2010} proved that the characterization given by Bloom was a lower bound, and provided the correct analysis to the false positive rate, although it is an intensive computation.% The probability of false positives has no impact on the correctness of the proposed protocol as discussed in Section \ref{sec:prot}



\section{Probabilistic soft-state using Counting Bloom Filters}
\label{sec:routing}

\subsection{Overview}

The proposed protocol assigns a locally computable probability of reaching any node in the network. To establish these probabilities throughout the network, nodes periodically exchange counting Bloom filters \cite{CountingBF00} that condense the topological information they have been able to collect. When a node receives a filter from a neighbor, it uses the \emph{addition} operation to merge its current topological information with the one contained in the received filter. Additionally, nodes use the \emph{degradation} operation to implement a probabilistic form of soft state, in which the information about the destinations is gradually lost as it grows stale and as it is being propagated away from the place where it was generated.

From the filters received from each neighbor, nodes compute a probability of reaching any destination $D$ through that particular neighbor. The gradients defined by these probabilities are further used to disseminate data packets in a store-carry-and-forward fashion to their intended destinations. A given data packet will be forwarded to a neighbor when the probability of reaching its intended destination plus a constant threshold, is larger than the probability of the current node. This way, packets tend to travel only to those regions of the network where it is more likely to find their intended destination. Please note that based on the probabilities assigned to nodes, many other forwarding strategies can be defined.

In the following sections, we present more detailed descriptions of the proposed operators and of the way the probabilities of reaching a destination are computed.

\subsection{Filter Operations}

A \emph{Counting Bloom Filter} $F$ is an array of $m$ counters that can take values from $0$ to $c$. Given a filter $F$, $F[x]$ denotes the value of the $x$-$th$ counter (with $x\in {0,...,m-1}$) contained in the filter. We use $\mathcal{F}_{m}^{c}$ to denote the set of every filter composed of $m$ counters with capacity $c$. We also denote the empty filter as $F_{\emptyset}^{}$, where all counters are set to $0$. The three operations defined over the filters are as follows.

The \emph{insert} operation is used to add the identity of a node into a Counting Bloom filter. It takes as parameters a filter $F$ and the identifier $a \in ADDR$ of a node (where $ADDR$ is the space of node identifiers in the network) and returns a new filter that contains the identity $a$. To insert the identity $a$ in a filter $F$, denoted as $F+a$, the value of $k$ independent hash functions ($h_i: ADDR \to \{0\dots m-1\}$ with $i \in \{1 \dots k\}$) is calculated to obtain the indexes of $k$ counters that will be set to the value of $c$. In this way, the operation defined by $F+a$ results in $F[h_i(a)] \gets c$ with $i  \in \{1 \dots k\}$. Note that this operation is different from the traditional insertion operation defined for Counting Bloom Filters where the counters are incremented in one unit.

\section{Routing using Counting Bloom Filters}
\subsection{Overview}
\label{sec:routingoverview}
The proposed protocol can assign a locally computable delivery probability to any node in the network with just a periodical exchange of counting Bloom Filters. The Bloom Filters condense the topological knowledge acquired by the node. When a node receives a filter from a neighbor, it uses the addition operation (Alg. \ref{alg:add}) to merge its current topological information with the one contained in the received filter. Additionally, nodes use the degradation operation to implement a probabilistic form of soft state, in which the information about the destinations deteriorates as it grows stale and as it propagates away from its original location modeling a space-time information loss.
With the received filters from the neighbors, each node can compute a delivery probability to any destination $D$ through that specific neighboring node. The probability gradient created by the information loss is further used to control the copy creation and the forwarding of the packets in a store-carry-and-forward fashion. We detail the criteria used to forward the packet in Section \ref{sec:prot}. Please note that based on the probabilities assigned to nodes, many other forwarding strategies can be defined. In the following sections, we present more detailed descriptions of the proposed operators and of the way the probabilities of reaching a destination are computed.


\subsection{Filter operations}
\label{sec:filterops}
A Counting Bloom Filter $F$ is an array of $m$ counters that can take values in the interval $[0,c]$ \cite{CountingBF00}. Given a filter $F$, $F[x]$ denotes the value of the $x$-th counter (with $x \in {0, \dots ,m-1}$) contained in the filter. We also denote the empty filter as $F_0$, where all counters set their values to zero. The three proposed operations over the filters are as follows.

The \emph{insert} operation is used to add the identity of a node into a Counting Bloom Filter. It takes as parameters a filter $F$ and the identifier $a\in ADDR$ of a node, $ADDR$ is the set of identifiers in the network. To insert a new element in a filter $F$, denoted as $F+a$, the value of $k$ independent hash functions ($h_i: ADDR \to \{0, \dots, m-1\}$ with $i \in \{1, \dots, k\}$) is calculated to obtain the indexes of $k$ counters that will be set to the max value of $c$. In this way, the operation defined by $F+a$ results in $F[h_i(a)] \gets c$ with $i  \in \{1, \dots, k\}$. Note that this operation is different from the traditional insertion operation defined for Counting Bloom Filters in \cite{CountingBF00} where the counters value gets incremented in one unit.

The \emph{degradation} operation is a unary operation that consists in decrementing in a single unit the value of each counter. This operation is denoted by  $\Delta: \mathcal{F}_{m}^{c} \to \mathcal{F}_{m}^{c} $ and is defined by the Algorithm \ref{alg:degrade}. The degradation operation is used to model information loss across space and time; either because it is growing stale or because the information is geographically away from its originating place.

\begin{algorithm}
	\begin{small}
		\caption{$\Delta_{p_{degrade}} (F)$}
		\label{alg:degrade}
		\begin{algorithmic}

			\For{$i=0 \to m-1$}
			\If{$F[i] > 0$}
			\State $F[i]\gets F[i]-1$; with a probability $p_{degrade}$
			\EndIf
			\EndFor
		\end{algorithmic}
	\end{small}
\end{algorithm}

The third operation is the binary \emph{addition} operation which is denoted by $\oplus: \mathcal{F}_{m}^{c} \times \mathcal{F}_{m}^{c} \to \mathcal{F}_{m}^{c}$. This operation combines the value of two filters $F_i$ and $F_j$ following the Algorithm \ref{alg:add}.

\begin{algorithm}
	\caption{Addition($F_i,F_j$)}
	\label{alg:add}
	\begin{small}
		\begin{algorithmic}

			\State{$F_x$ is a new filter;}
			\For{$l=0 \to m-1$}
			\State $F_x [l] \gets \max\{F_i [l],F_j [l]\}$
			\EndFor
			\State return $F_x$
		\end{algorithmic}
	\end{small}
\end{algorithm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Probabilistic Soft State}
Each node $i$ will keep two filters that store its own topological information, namely, $F_{i}^{*}$ and $F_{i}^{t}$. $F_{i}^{*}$ is constant and is defined as $F_{\emptyset}+i$, whereas $F_{i}^{t}$ is time dependent and its content changes because of the updates received from neighboring nodes and from a periodical degradation process. When a node $i$ receives a filter $F_{j}^{t}$ from its neighbor $j$, it updates the value of its own filter $F_{i}^{t+1}$ according to Eq. (\ref{eq:update}). Please note that in Eq. (\ref{eq:update}) the filter received from neighbor $j$ is first degraded to reflect the loss of information and then added to the filter of node $i$.

\begin{equation}\label{eq:update}
F_{i}^{t+1} \gets \Delta_{p_{degrade}} (F_{j}^{t})\oplus F_{i}^{t}
\end{equation}

The filter $F_{i}^{t}$ periodically degrades according to Eq. \ref{eq:periodegrad} every $T_d$ seconds as a way to implement a form \emph{probabilistic soft state}. The concept of probabilistic soft state is similar to the concept of \emph{Weak State} proposed in \cite{acer2010weak} in the sense that both provide probabilistic hints regarding the location of the destinations. Unlike Weak State, our proposal does not rely on geographic information; instead, the probabilistic soft state uses the topological information to establish the gradient.
Moreover, the probabilistic soft state has a spatial/temporal nature because it captures the fact that information deteriorates as times goes by, but also as it traverses the network away from the generating place.
%away from the originating place?

\begin{equation}\label{eq:periodegrad}
F_{i}^{t+1} \gets \Delta_{p_{degrade}} (F_{i}^{t})\oplus F_{i}^{*}
\end{equation}

The probability of reaching a destination through node $i$ can be obtained using Eq. (\ref{eq:delivprob}). The probability assigned to each node in the network is precisely the one that defines the probabilistic gradient used to reach the destinations. Note that under this scheme, the probability of reaching node $i$ through itself is always $1$.

\begin{equation}\label{eq:delivprob}
pr_{i}^{D}\gets \frac{\sum_{\forall x|F_{D}^{*}[x]>0}^{}F_{i}^{t}[x]}{kc}
\end{equation}

The connectedness of a given node can be inferred calculating the saturation in its counting Bloom filter with Eq. \ref{eq:saturation}. Note that when the saturation tends to zero, the node is in a sparse zone of the network, and when it approaches one, the node has at least a path to a highly connected component of the network.
\begin{equation}\label{eq:saturation}
S_i \gets \frac{\sum_{g=0}^{k}F_i[g]}{mc}
\end{equation}

The heuristic to decide whether forward a message or not, is done as a simple threshold expressed in Eq. \ref{eq:threshold}
\begin{equation}\label{eq:threshold}
pr_{i}^{D} < pr_{j}^{D}+\rho
\end{equation}






\subsection{Complexity}\label{sec:complexity}
For a given network of $n$ nodes, with $m$ created messages, we the complexity, both in space and time. Having a low complexity in either regard influences positively the performance in constrained environments by reducing local computing power, storage and network capacity needed for correct protocol operation.
We present an evaluation of time and space complexity; we focus on the decision process to forward messages across the network and the information needed to do so. The $\gamma_{discover}$ operation is used as space complexity indicator since most of the algorithms use this operation to propagate topological knowledge across the network. For time complexity we evaluate the operations necessary to decide whether a node should forward a message or not. In Table \ref{tab:complexity} we present a comparison between the complexity of the evaluated algorithms.

\begin{table}[!t]
	\renewcommand{\arraystretch}{1.3}
	\caption{Complexity}
	\label{tab:complexity}
	\centering
	\begin{tabular}{c||c||c}
		\hline
		\bfseries Algorithm & \bfseries Space & \bfseries Time\\
		\hline\hline
		Proposal & $O(k)$ & $O(n)$ \\
		\hline
		MaxPROP &  $O(n^2)$ & $O(n^2)$ \\
		\hline
		SeeR &     $O(n)$ &  $N/A$ \\
		\hline
		PRoPHET  & $O(n)$ & $O(n)$ \\
		\hline
		Epidemic & $O(m)$ & $O(k)$ \\
		\hline
		Epidemic with Oracle & $O(m)$ & $O(k)$ \\
		\hline
		Spray n' Wait  & $O(k)$ & $O(n)$ \\
		\hline
	\end{tabular}
\end{table}


We must note that our proposal improves significantly the metrics used to measure the performance of said protocols in all environments respectively to those algorithms of similar complexity and achieving similar results to algorithms of higher space and time complexity.


%Simula un comentario de bloque, omitiendo todo el contenido hasta \fi de la salida
\iffalse

\subsection{Protocol Description}
\label{sec:prot}

We define the following type of messages that are exchanged by nodes as they opportunistically meet. The protocol itself forms as a sequence of this messages.

\begin{itemize}
	\item BLF: Contains filter $F_{i}^{t}$ which condenses the topological information collected by node $i$.
	\item SUV: Array of packet identifiers. It is used to publish the set of messages stored at this node.
	\item REQ: Array of packet identifiers. Set of messages requested by this node.
	\item DAT: Message that contains a set of data packets.
\end{itemize}

Every node $j$ broadcasts a hello message $m_{BLF}$ containing his own Bloom filter $F_{j}^{t}$ every $t_h$ seconds. As shown in Algorithm \ref{alg:recBloom}, upon reception of a message $m_{BLF}$ from node $j$, node $i$ proceeds to update its internal filter using Eq. \ref{eq:update} and to compute the new delivery probabilities through node $j$ using Eq. \ref{eq:delivprob}. Then, node $i$ adds the identifiers of the data packets with destination $D$, such that the delivery probability at node $j$ is strictly greater than the delivery probability at node $i$ plus a threshold (Eq. \ref{eq:threshold}),  to the list $L_{suv}$ which $i$ sends in a $m_{SUV}$ message to $j$. This way, the packets listed in $L_{suv}$ are such that their destinations are more likely to be reached through $j$ than through $i$. The threshold $\rho$ changes the behavior of the protocol in sparse areas of the network to a more epidemic-like approach.

\begin{algorithm}
	\caption{Bloom Filter Message} \label{alg:recBloom}
	\begin{small}
		\begin{algorithmic}
			\WhenStart[$m_{BLF}$]{j}
			\State $L_{suv} \gets {\emptyset}$
			\State $F_i^t \gets F_i^t \oplus F_i^*$
			\State $F_i^{t+1}\gets \Delta_{p_{degrade}}(F_j^t)\oplus F_i^t$

			\For {Message m $\in$ $BUFFER_i$}
			\State $D \gets m.destination$
			\State $pr_{i}^{D}\gets \frac{\sum_{\forall x|F_{D}^{*}[x]>0}^{}F_{i}^{t+1}[x]}{km}$
			\State $pr_{j}^{D}\gets \frac{\sum_{\forall x|F_{D}^{*}[x]>0}^{}F_{j}^{t}[x]}{km}$
			\State $\rho \gets e^{-\tau S_i}$
			\If{$pr_{i}^{D} < pr_{j}^{D}+\rho$ $\vee$  $pr_{j}^{D} = 1$}
			\State $L_{suv} \gets L_{suv} \cup \{m.id\}$
			\EndIf
			\EndFor
			\State send(j, $L_{suv}$)
			\WhenEnd
		\end{algorithmic}
	\end{small}
\end{algorithm}

As described by the pseudocode of Algorithm \ref{alg:suv}, when node $i$ receives the message $m_{SUV}$,  it creates a new list $L_{req}$ that contains the packets listed in $L_{suv}$  that has not seen so far. Then, node $i$ sends a message $m_{REQ}$ back to $j$ requesting the messages in $L_{req}$.

\begin{algorithm}
	\caption{Summary Message}
	\label{alg:suv}
	\begin{small}
		\begin{algorithmic}
			\WhenStart[$m_{SUV}$]{j}
			\State $L_{req} \gets \{\emptyset\}$
			\For {Identifier $m_i$ $\in L_{suv}$}
			\If {$m_i \notin BUFFER_i$}
			\State $L_{req} \gets L_{req} \cup \{m_i\}$
			\EndIf
			\EndFor
			\State send(j, $L_{req}$)
			\WhenEnd
		\end{algorithmic}
	\end{small}
\end{algorithm}


When node $j$ receives the list $L_{req}$, it sends a message $m_{DAT}$ back to $i$ containing every data packet requested by $i$. This action is a series of consecutive $\gamma_{(j,i)}$ operations. Lastly, when node $i$ receives the data packets, it passes to upper layers those packets for which is the intended destination and stores the others in its internal buffer. This latter action is equivalent to a series of $\gamma_{store}(i)$ operations.

As already mentioned, every node $i$,  periodically degrades its own filter $F_{i}^{t}$ using Eq. (\ref{eq:periodegrad}). This way, node $i$ will eventually lose any state regarding destinations that have moved far away or to a different network partition.


%End block comment
\fi

It is important to point out that in the presence of false positives, data packets can still be disseminated towards regions of the network that do not contain the destination; however, this will not prevent the proposed routing protocol to also disseminate the packets towards the real positive, namely, towards the intended destination. Therefore, while a false positive does increase the cost of delivering a data packet, it does not prevent the data packet from reaching its destination. In the worst case, our proposed protocol will behave like a pure epidemic dissemination protocol but with the extra overhead induced by exchanging the Bloom filters.


\section{Experimental setup}\label{sec:experimental}
We implemented the proposed algorithm in the Opportunistic Network Environment simulator (ONE) \cite{theone} to evaluate its performance. We use three scenarios that differ according to the mobility model: real pedestrian traces from the Infocom'06 dataset \cite{one-infocom}, MANET, and VANET scenarios.

In the pedestrian scenario, we used the traces collected by volunteers wearing Bluetooth beacons across 4 days during INFOCOM 2006 conferences \cite{dataset-infocom} and reformatted to be used with the ONE Simulator \cite{one-infocom}. The traces consists of 78 mobile nodes with a transmission range of 30 meters and 20 stationary nodes with a transmission range of 100 meters approximately. This scenario evaluates the effect of buffer size, time-to-live of the messages and traffic load across the network.

For the MANET scenario, the nodes were assigned a network interface with a transmission range of 100 meters and a link capacity of 11 Mbps. We used the Random waypoint mobility model to test the performance in non-social networks, this in a square area of 15 Kilometers by 15 Kilometers and with speeds uniformly chosen at random in the interval $[10, 50]Km/h$.

Finally, we chose a set with the main streets of Mexico City to simulate an urban mobile environment for the VANET scenario; the dimensions of the bounding box are 35 Km x 25 Km. The mobility model for this scenario lets a node choose a point in the map at random, then uses the Dijkstra algorithm to map the shortest route to that destination and assigns a velocity also chosen uniformly at random in the specified interval. Other than the mobility model and simulation area the parameters were the same as in the MANET scenario.

For the MANET and VANET scenario, we varied the node density and message creation rate, to simulate different network conditions such as connectedness,  traffic load, buffer saturation.

The parameters for our proposed protocol were chosen empirically. Table \ref{tab:param} presents this parameters in detail.

\begin{table}[!t]
	\renewcommand{\arraystretch}{1.3}
	\caption{Simulation parameters}
	\label{tab:param}
	\centering
	\begin{tabular}{c||c}
		\hline
		\bfseries Parameter & \bfseries Value\\
		\hline\hline
		$m$& 128 \\
		\hline
		$k$& 8 \\
		\hline
		$c$& 32 \\
		\hline
		$p_{degrade}$& 1 \\
		\hline
		$T_d$ & 300 \\
		\hline
		$\tau$ & 4 \\
		\hline
	\end{tabular}
\end{table}


	\subsection{Metrics}
	To evaluate the performance of all routing algorithms we chose four main metrics: packet delivery ratio, latency, overhead ratio, and delivery speed. The packet delivery ratio is the proportion between messages created and  those delivered to its destination, latency is the average time for a message to reach its destination, the overhead ratio is defined as the number of copies created divided by the messages delivered, i.e., the average number of copies that are needed to deliver a message.

	To better describe the performance of the algorithm we propose the fourth metric as the average travel speed for the messages, i.e., the physical distance between origin and destination at the time of the message is created divided by the time it took to reach its destination. This metric is introduced to distinguish the cases where a message is created in the vicinity of its destination and consequentially offers a low latency with a high delivery probability.



\section{Results}
In this section, we present the experimental results obtained and analyze the performance. In each Figure, a row represents a metric indicated by the y-axis; each column indicates a variation of the experiment and its value as a header. Finally, the x-axis of each plot corresponds to a network capacity available variation, going from high traffic to a low traffic scenario as the axis values increases.

	\subsection{Pedestrian}
	 In Figure \ref{fig:pedestrian_results} we show that the performance of our proposal delivers the most messages in the network even in restricted environments of high network load and small buffers, the ideal version of Epidemic is the only better alternative. Although our proposal positions itself in a 4th place regarding latency, it also remains competitive due to its stability across the network and environment variations; in this case SeeR, a protocol that focuses on latency optimization remains as the fastest algorithm in this scenario. Note that the performance of other algorithms like PRoPHET and Epidemic is competitive only when the network conditions are low demanding.

	 As this scenario uses real human mobility data, it is safe to assume that exists a topology with social behavior like clustering of particular nodes, periodic movement, and common points of interest. We note that the proposed algorithm exploits this structure through the probabilistic soft-state that is established through the social interactions of the nodes and yet work as a stochastic algorithm.
	 \begin{figure*}[!t]
	 	\centering
	 	\subfloat[a]{\input{assets/CRAWDAD_latency.pgf} \label{fig:pedestrianLatency}}
	 	\hfil
 		\subfloat[b]{\input{assets/CRAWDAD_pdr.pgf} \label{fig:pedestrianPDR}}
 		\hfil
	 	\subfloat[c]{\input{assets/CRAWDAD_or.pgf} \label{fig:pedestrianOverhead}}
	 	\hfil
	 	\input{assets/CRAWDAD_legend.pgf}
	 	\caption{Results for the pedestrian mobility
	 	scenario.\ref{fig:pedestrianLatency} shows the average latency in the network. \ref{fig:pedestrianPDR} shows the packet delivery ratio. \ref{fig:pedestrianOverhead} shows the overhead ratio.}
	    \label{fig:pedestrian_results}
	 \end{figure*}

	\subsection{VANET}
	In this scenario we note a common trend amongst the routing algorithms, they all achieve their best performance in a dense network with low traffic as it is shown in Figure \ref{fig:VANET_results}.

	Our proposal achieved a similar performance to MaxPROP, both in latency and packet delivery ratio, but as discussed in Section \ref{sec:complexity} the complexity is significantly reduced.

	We considered this scenario the worst for our approach due to the several adversarial examples to the algorithm and how the information is propagated. Nonetheless it shows a superior performance compared to state of the art algorithms.
	\begin{figure*}[!t]
		\centering
		\subfloat{\input{assets/VANET_latency.pgf}	\label{fig:VANET_Latency}}
		\hfil
		\subfloat{\input{assets/VANET_pdr.pgf}	\label{fig:VANET_PDR}}
		\hfil
		\subfloat{\input{assets/VANET_or.pgf} \label{fig:VANET_Overhead}}
		\hfil
		\input{assets/VANET_legend.pgf}
		\caption{Results for the VANET mobility scenario. \ref{fig:VANET_PDR} shows the packet delivery ratio. \ref{fig:VANET_Latency} shows the average latency in the network and \ref{fig:VANET_Overhead} shows the overhead ratio.}
		\label{fig:VANET_results}
	\end{figure*}

	\subsection{MANET}
	In this scenario we wanted to asses the performance in an environment where no mobility patterns could be exploited. The results
	\begin{figure*}[!t]
		\subfloat{\input{assets/MANET_latency.pgf}	\label{fig:MANET_Latency}}
		\hfil
		\subfloat{\input{assets/MANET_pdr.pgf}	\label{fig:MANET_PDR}}
		\hfil
		\subfloat{\input{assets/MANET_or.pgf}	\label{fig:MANET_Overhead}}
		\hfil
		\input{assets/MANET_legend.pgf}
		\caption{Results for the MANET mobility scenario. \ref{fig:MANET_PDR} shows the packet delivery ratio. \ref{fig:MANET_Latency} shows the average latency in the network and \ref{fig:MANET_Overhead} shows the overhead ratio.}
		\label{fig:MANET_results}
	\end{figure*}

	\section{Conclusion}
	In this paper, we proposed a new protocol for routing in intermittently connected mobile networks that use the concept of \emph{probabilistic soft state} to assign to every node in the network probabilities of reaching any destination. The probabilistic soft state is defined in terms of the content of counting Bloom filters that condense the topological information that nodes have collected from other nodes as they move and that is used to compute the probabilities of reaching the destinations. These probabilities reflect the amount of information that the nodes have regarding the different destinations. To model the process in which nodes acquire and loss topological information, we defined two novel operations over counting Bloom filters, namely, the binary \emph{addition} operation and the unary \emph{degradation} operation. The addition operation is used to reflect  the acquisition of new information, and the unary operation is used to reflect the loss of information, either because it is getting stale or because it is being propagated away from the place where it was generated. Then, nodes use a greedy forwarding strategy that follows the gradients defined by these probabilities to reach the intended destinations.

	The proposed scheme has the advantage of establishing probabilistic gradients simultaneously for all the destinations. This contrasts with previous proposals that require the dissemination of control information for every destination node, which is not scalable. In the proposed scheme, nodes periodically exchange constant-size ``hello'' messages with their one-hop neighbors. Therefore, the protocol's network complexity is constant in terms of the network size. Moreover, the proposed scheme has the advantage that it combines in a simple way the concepts of the probability of encounter and hop distance towards the destinations. When the network is connected, the probabilistic gradients are similar to the traditional gradients based on hop distances, but when the network is partitioned, the gradients are similar to those that compute a probability of encounter based on the time nodes last met. This way, the proposed algorithm can work in more general scenarios than previous protocols that exploit the ``social behavior'' of nodes.

	In the presence of false positives in the Bloom filters, data packets can be disseminated towards regions of the network that do not contain the destination, however, this will not prevent the proposed protocol also to disseminate the packets towards the intended destination. We observed this situation in the VANET scenario, where due to a large number of nodes, the probability of false positive is quite high.

	We also note that with our approach, the time and frequency of contacts of a specific node is up to a certain point anonymous. Take for example a malicious node that wants to reveal the list of frequent contacts of a particular node. The attacker should know beforehand the identities of the frequent nodes and test each one against the victim node's filter; even with that information available, the Bloom filter could yield a false positive. The obfuscation of frequent contacts is an essential factor for privacy-enabled networks or applications, especially for human-related ones.

	Our protocol proves to be a viable solution for resource-constrained devices that not only is competitive with the state of the art performance, but it also has consistent performance in different scenarios, exploiting social characteristics if available.

	\bibliography{IEEEabrv,icn_papers}
\end{document}
