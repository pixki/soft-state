TEX_FILES="main.tex"
BIBLIOGRAPHY="icn_papers.bib"


clean:
	find . -name "*.aux" -exec rm {} \;
	find . -name "*.log" -exec rm {} \;
	find . -name "*.synctex" -exec rm {} \;
	find . -name "*.blg" -exec rm {} \;
	find . -name "*.bbl" -exec rm {} \;


pdf:
	pdflatex $(TEX_FILES)
	bibtex main
	pdflatex $(TEX_FILES)
	pdflatex $(TEX_FILES)


zip: pdf
	rm -rf soft-state
	mkdir soft-state
	cp -r assets soft-state
	cp $(TEX_FILES) soft-state
	cp $(BIBLIOGRAPHY) soft-state
	cp main.pdf soft-state
	zip -r soft-state.zip ./soft-state
	rm -rf soft-state


.PHONY: pdf clean zip
